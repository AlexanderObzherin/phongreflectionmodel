#include "ObservableObject.h"
#include "Game.h"
#include "GameException.h"
#include "VectorHelper.h"
#include "MatrixHelper.h"

#include "Camera.h"
#include "Model.h"
#include "Mesh.h"
#include "Utility.h"

#include "RenderStateHelper.h"
#include <SpriteBatch.h>
#include <SpriteFont.h>
#include <sstream>

namespace Library
{
	RTTI_DEFINITIONS( ObservableObject )

	ObservableObject::ObservableObject( Game& game, Camera& camera )
	:
	DrawableGameComponent( game, camera ),

	mPosition( XMFLOAT3( 0.0f, 0.0f, 0.0f ) ),
	mAverVelocity( XMFLOAT3( 0.0f, 0.0f, 0.0f ) ),
	scaleCoefficient( 1.0f )
	{
	}

	ObservableObject::~ObservableObject()
	{
	}

	const XMFLOAT3& ObservableObject::Position() const
	{
		return mPosition;
	}

	const XMFLOAT3& ObservableObject::Velocity() const
	{
		return mAverVelocity;
	}

	float ObservableObject::GetScaleCoeff()
	{
		return scaleCoefficient;
	}

	void ObservableObject::SetScaleCoeff( float scale )
	{

	}

	void ObservableObject::Update( const GameTime& gameTime )
	{

	}
}
