#pragma once

#include "Camera.h"


namespace Library
{
	class Keyboard;
	class Mouse;

	class ObservableObject;

	enum CameraViewPosition
	{
		bottom = 0,
		angleMinus45deg,
		side,
		angle45deg,
		top
	};

	class ThirdPersonCamera : public Camera
	{
		RTTI_DECLARATIONS( ThirdPersonCamera, Camera )

	public:
		ThirdPersonCamera( Game& game );
		ThirdPersonCamera( Game& game, float fieldOfView, float aspectRatio, float nearPlaneDistance, float farPlaneDistance );

		virtual ~ThirdPersonCamera();

		const Keyboard& GetKeyboard() const;
		void SetKeyboard( Keyboard& keyboard );

		const Mouse& GetMouse() const;
		void SetMouse( Mouse& mouse );

		float& MouseSensitivity();
		float& RotationRate();
		float& MovementRate();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;

		XMVECTOR linearMovingVector( XMFLOAT3 targetPoint );
		void radialMovingTo( float mTargetPolarAngle );
		void RadialRotation( float polarAngle, float azimuthAngle );
		void RadialRotation( float polarAngle, float azimuthAngle, XMFLOAT3 mPivotPoint );
		void MouseFreeRotation();
		void MoveForewardViewVector( const GameTime& gameTime );
		CameraViewPosition GetCameraViewPosition() const;
		void SetObservableObject( ObservableObject* object );
		
		static const float DefaultMouseSensitivity;
		static const float DefaultRotationRate;
		static const float DefaultMovementRate;

		std::string DrawCameraViewPosString() const;

	public:

		ObservableObject* mObservableObject;
		const XMFLOAT3* mObsrevablePoint;
		XMFLOAT3 mTargetPoint;

	protected:
		XMFLOAT3 mViewVector;

		float mMouseSensitivity;
		float mRotationRate;
		float mMovementRate;

		float mRotationAngle;
		float mRotationAngleVelocityCoeff;
		float mCurrentPolarAngle;
		float targetPolarAngle;

		float mCameraDistance;

		Keyboard* mKeyboard;
		Mouse* mMouse;
		CameraViewPosition mCameraViewPosition;

		static const std::string DrawCameraViewPosNames[];

	private:
		ThirdPersonCamera( const ThirdPersonCamera& rhs );
		ThirdPersonCamera& operator=(const ThirdPersonCamera& rhs);
	};
}

