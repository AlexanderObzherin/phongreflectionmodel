#pragma once

#include "DrawableGameComponent.h"

using namespace Library;

namespace Library
{
	class Technique;
	class Effect;
	class DirectionalLight;
	class Keyboard;
	class RenderStateHelper;
}

namespace DirectX
{
	class SpriteBatch;
	class SpriteFont;
}

namespace Rendering
{
	class PhongReflectionModelMaterial;

	enum DrawMode
	{
		AmbientDiffuseSpecularMode = 0,
		AmbientDiffuseMode,
		AmbientMode,
		DrawModeEnd
	};

	class AcornModel : public DrawableGameComponent
	{
		RTTI_DECLARATIONS( AcornModel, DrawableGameComponent )

	public:
		AcornModel( Game& game, Camera& camera, const DirectionalLight& directionalLightSource );
		~AcornModel();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;



	private:
		AcornModel();
		AcornModel( const AcornModel& rhs );
		AcornModel& operator=( const AcornModel& rhs );

		void UpdateAmbientLight( const GameTime& gameTime );
		void UpdateSpecularLight( const GameTime& gameTime );

		std::string DrawModeString() const;
		DrawMode GetDrawMode() const;
		void SetDrawMode( Rendering::DrawMode drawMode );

		static const float LightModulationRate;
		static const XMFLOAT2 LightRotationRate;

		std::vector<Technique*>::const_iterator techniqueIterator;

		XMFLOAT3 mPosition;

		Effect* mEffect;
		PhongReflectionModelMaterial* mMaterial;
		ID3D11ShaderResourceView* mTextureShaderResourceView;
		ID3D11Buffer* mVertexBuffer;
		ID3D11Buffer* mIndexBuffer;
		UINT mIndexCount;

		XMCOLOR mAmbientColor;
		XMCOLOR mSpecularColor;
		float mSpecularPower;

		const DirectionalLight& mDirectionalLightSource;
		
		Keyboard* mKeyboard;
		XMFLOAT4X4 mWorldMatrix;
		
		RenderStateHelper* mRenderStateHelper;
		SpriteBatch* mSpriteBatch;
		SpriteFont* mSpriteFont;

		Rendering::DrawMode mDrawMode;
		static const std::string DrawModeDisplayNames[];
	};
}
