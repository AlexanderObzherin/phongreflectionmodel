#pragma once

#include "Common.h"
#include "Game.h"

using namespace Library;

namespace Library
{
	class Keyboard;
	class Mouse;
	class FirstPersonCamera;
	class FpsComponent;
	class RenderStateHelper;
	class Grid;
	class ProxyModel;
	class Skybox;
	class DirectionalLight;
}

namespace DirectX
{
	class SpriteBatch;
	class SpriteFont;
}

namespace Rendering
{
	class AcornModel;

	class RenderingGame : public Game
	{
	public:
		RenderingGame( HINSTANCE instance, const std::wstring& windowClass, const std::wstring& windowTitle, int showCommand );
		~RenderingGame();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

		void UpdateDirectionalLight( const GameTime& gameTime );

	protected:
		virtual void Shutdown() override;

	private:
		static const XMVECTORF32 BackgroundColor;

		static const float LightModulationRate;
		static const XMFLOAT2 LightRotationRate;

		LPDIRECTINPUT8 mDirectInput;
		Keyboard* mKeyboard;
		Mouse* mMouse;
		FirstPersonCamera* mCamera;
		FpsComponent* mFpsComponent;
		RenderStateHelper* mRenderStateHelper;
		Grid* mGrid;
		ProxyModel* mProxyModel;
		Skybox*mSkybox;

		AcornModel* mAcorn;
		DirectionalLight* mDirectionalLight;

		SpriteBatch* mSpriteBatch;
		SpriteFont* mSpriteFont;
	};
}