#include "RenderingGame.h"
#include "GameException.h"
#include "Keyboard.h"
#include "Mouse.h"
#include "FpsComponent.h"
#include "ColorHelper.h"
#include "VectorHelper.h"
#include "FirstPersonCamera.h"
#include "RenderStateHelper.h"
#include "RasterizerStates.h"
#include "SamplerStates.h"
#include "Grid.h"
#include "DirectionalLight.h"
#include "ProxyModel.h"
#include "Skybox.h"

#include <sstream>
#include <iomanip>
#include <SpriteBatch.h>
#include <SpriteFont.h>

#include "AcornModel.h"


namespace Rendering
{
	const XMVECTORF32 RenderingGame::BackgroundColor = ColorHelper::CornflowerBlue;

	const float RenderingGame::LightModulationRate = UCHAR_MAX;
	const XMFLOAT2 RenderingGame::LightRotationRate = XMFLOAT2( XM_2PI, XM_2PI );

	RenderingGame::RenderingGame( HINSTANCE instance, const std::wstring& windowClass, const std::wstring& windowTitle, int showCommand )
		:
		Game( instance, windowClass, windowTitle, showCommand ),
		mFpsComponent( nullptr ),
		mDirectInput( nullptr ),
		mKeyboard( nullptr ),
		mMouse( nullptr ),
		mRenderStateHelper( nullptr ),
		mSpriteBatch( nullptr ),
		mSpriteFont( nullptr ),
		mGrid( nullptr ),
		mProxyModel( nullptr ),
		mSkybox( nullptr ),
		mDirectionalLight( nullptr ),
		mAcorn( nullptr )
	{
		mDepthStencilBufferEnabled = true;
		mMultiSamplingEnabled = true;
	}

	RenderingGame::~RenderingGame()
	{
	}

	void RenderingGame::Initialize()
	{
		if( FAILED( DirectInput8Create( mInstance, DIRECTINPUT_VERSION, IID_IDirectInput8, (LPVOID*)&mDirectInput, nullptr ) ) )
		{
			throw GameException( "DirectInput8Create() failed" );
		}

		mKeyboard = new Keyboard( *this, mDirectInput );
		mComponents.push_back( mKeyboard );
		mServices.AddService( Keyboard::TypeIdClass(), mKeyboard );

		mMouse = new Mouse( *this, mDirectInput );
		mComponents.push_back( mMouse );
		mServices.AddService( Mouse::TypeIdClass(), mMouse );

		mCamera = new FirstPersonCamera( *this );
		mComponents.push_back( mCamera );
		mServices.AddService( Camera::TypeIdClass(), mCamera );

		mFpsComponent = new FpsComponent( *this );
		mFpsComponent->Initialize();

		//mGrid = new Grid( *this, *mCamera );
		//mComponents.push_back( mGrid );

		mSkybox = new Skybox( *this, *mCamera, L"Content\\Textures\\TropicalSunnyDay.dds", 10.0f );
		mComponents.push_back( mSkybox );

		mDirectionalLight = new DirectionalLight( *this );

		mProxyModel = new ProxyModel( *this, *mCamera, "Content\\Models\\DirectionalLightProxy.obj", 0.5f );

		mProxyModel->Initialize();
		mProxyModel->SetPosition( 10.0f, 10.0, 0.0f );
		mProxyModel->ApplyRotation( XMMatrixRotationY( XM_PIDIV2 ) );
		mProxyModel->ApplyRotation( XMMatrixRotationX( -XM_PIDIV2 ) );

		RasterizerStates::Initialize( mDirect3DDevice );
		SamplerStates::BorderColor = ColorHelper::Black;
		SamplerStates::Initialize( mDirect3DDevice );

		mAcorn = new AcornModel( *this, *mCamera, *mDirectionalLight );
		mComponents.push_back( mAcorn );

		mRenderStateHelper = new RenderStateHelper( *this );
	
		Game::Initialize();

		XMMATRIX rotationAroundX = XMMatrixRotationX( -0.150f );
		mCamera->ApplyRotation( rotationAroundX );
		mCamera->SetPosition( 0.0f, 20.0f, 25.0f );

		mSpriteBatch = new SpriteBatch( mDirect3DDeviceContext );
		mSpriteFont = new SpriteFont( mDirect3DDevice, L"Content\\Fonts\\TimesNewRoman_12_bold.spritefont" );
	}

	void RenderingGame::Shutdown()
	{
		DeleteObject( mAcorn );
		DeleteObject( mSkybox );
		DeleteObject( mProxyModel );
		DeleteObject( mGrid );
		DeleteObject( mRenderStateHelper );
		DeleteObject( mKeyboard );
		DeleteObject( mMouse );
		DeleteObject( mFpsComponent );
		DeleteObject( mCamera );

		ReleaseObject( mDirectInput );
		RasterizerStates::Release();
		SamplerStates::Release();

		Game::Shutdown();
	}

	void RenderingGame::Update( const GameTime &gameTime )
	{
		mFpsComponent->Update( gameTime );

		//mProxyModel->Update( gameTime );

		UpdateDirectionalLight( gameTime );

		if( mKeyboard->WasKeyPressedThisFrame( DIK_ESCAPE ) )
		{
			Exit();
		}

		Game::Update( gameTime );
	}

	void RenderingGame::Draw( const GameTime &gameTime )
	{
		mDirect3DDeviceContext->ClearRenderTargetView( mRenderTargetView, reinterpret_cast<const float*>(&BackgroundColor) );
		mDirect3DDeviceContext->ClearDepthStencilView( mDepthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0 );

		//mProxyModel->Draw( gameTime );

		Game::Draw( gameTime );

		mRenderStateHelper->SaveAll();
		mFpsComponent->Draw( gameTime );

		mSpriteBatch->Begin();
		std::wostringstream helpLabel;
		helpLabel << L"Camera moving ( A, D, W, S): \n";
		helpLabel << L"Camera rotation ( Mouse left ): \n";

		mSpriteFont->DrawString( mSpriteBatch, helpLabel.str().c_str(), XMFLOAT2( 0.0f, 30.0f ) );
		
		mSpriteBatch->End();
		mRenderStateHelper->RestoreAll();

		HRESULT hr = mSwapChain->Present( 0, 0 );
		if( FAILED( hr ) )
		{
			throw GameException( "IDXGISwapChain::Present() failed.", hr );
		}
	}

	void RenderingGame::UpdateDirectionalLight( const GameTime& gameTime )
	{
		static float directionalIntensity = mDirectionalLight->Color().a;
		float elapsedTime = (float)gameTime.ElapsedGameTime();

		// Update directional light intensity		
		if( mKeyboard->IsKeyDown( DIK_HOME ) && directionalIntensity < UCHAR_MAX )
		{
			directionalIntensity += LightModulationRate * elapsedTime;

			XMCOLOR directionalLightColor = mDirectionalLight->Color();
			directionalLightColor.a = (UCHAR)XMMin<float>( directionalIntensity, UCHAR_MAX );
			mDirectionalLight->SetColor( directionalLightColor );
		}
		if( mKeyboard->IsKeyDown( DIK_END ) && directionalIntensity > 0 )
		{
			directionalIntensity -= LightModulationRate * elapsedTime;

			XMCOLOR directionalLightColor = mDirectionalLight->Color();
			directionalLightColor.a = (UCHAR)XMMax<float>( directionalIntensity, 0.0f );
			mDirectionalLight->SetColor( directionalLightColor );
		}

		// Rotate directional light
		XMFLOAT2 rotationAmount = Vector2Helper::Zero;
		if( mKeyboard->IsKeyDown( DIK_LEFTARROW ) )
		{
			rotationAmount.x += LightRotationRate.x * elapsedTime;
		}
		if( mKeyboard->IsKeyDown( DIK_RIGHTARROW ) )
		{
			rotationAmount.x -= LightRotationRate.x * elapsedTime;
		}
		if( mKeyboard->IsKeyDown( DIK_UPARROW ) )
		{
			rotationAmount.y += LightRotationRate.y * elapsedTime;
		}
		if( mKeyboard->IsKeyDown( DIK_DOWNARROW ) )
		{
			rotationAmount.y -= LightRotationRate.y * elapsedTime;
		}

		XMMATRIX lightRotationMatrix = XMMatrixIdentity();
		if( rotationAmount.x != 0 )
		{
			lightRotationMatrix = XMMatrixRotationY( rotationAmount.x );
		}

		if( rotationAmount.y != 0 )
		{
			XMMATRIX lightRotationAxisMatrix = XMMatrixRotationAxis( mDirectionalLight->RightVector(), rotationAmount.y );
			lightRotationMatrix *= lightRotationAxisMatrix;
		}

		if( rotationAmount.x != 0.0f || rotationAmount.y != 0.0f )
		{
			mDirectionalLight->ApplyRotation( lightRotationMatrix );
			mProxyModel->ApplyRotation( lightRotationMatrix );
		}
	}
}