#include "AcornModel.h"
#include "PhongReflectionModelMaterial.h"
#include "Game.h"
#include "GameException.h"
#include "VectorHelper.h"
#include "MatrixHelper.h"
#include "ColorHelper.h"
#include "Camera.h"
#include "Model.h"
#include "Mesh.h"
#include "Utility.h"
#include "DirectionalLight.h"
#include "Keyboard.h"
#include <WICTextureLoader.h>

#include "RenderStateHelper.h"

#include <iomanip>
#include <SpriteBatch.h>
#include <SpriteFont.h>
#include <sstream>

namespace Rendering
{
	RTTI_DEFINITIONS( AcornModel )

	const float AcornModel::LightModulationRate = UCHAR_MAX;
	const XMFLOAT2 AcornModel::LightRotationRate = XMFLOAT2( XM_2PI, XM_2PI );

	const std::string AcornModel::DrawModeDisplayNames[] = { "Ambient + Diffuse + Specular", "Ambient + Diffuse", "Ambient" };

	AcornModel::AcornModel( Game& game, Camera& camera, const DirectionalLight& directionalLightSource )
		:
		DrawableGameComponent( game, camera ),
		mEffect( nullptr ),
		mMaterial( nullptr ),
		mTextureShaderResourceView( nullptr ),
		mVertexBuffer( nullptr ),
		mIndexBuffer( nullptr ),
		mIndexCount( 0 ),
		mKeyboard( nullptr ),
		mAmbientColor( 1.0f, 1.0f, 1.0f, 0.0f ),
		mDirectionalLightSource( directionalLightSource ),
		mSpecularColor( 1.0f, 1.0f, 1.0f, 1.0f ), 
		mSpecularPower( 25.0f ),
		mWorldMatrix( MatrixHelper::Identity ),
		mPosition( 0.0f, 12.0f, 0.0f ),
		
		mRenderStateHelper( nullptr ),
		mSpriteBatch( nullptr ),
		mSpriteFont( nullptr ),
		mDrawMode( AmbientDiffuseSpecularMode )
	{
	}

	AcornModel::~AcornModel()
	{
		DeleteObject( mSpriteFont );
		DeleteObject( mSpriteBatch );
		DeleteObject( mRenderStateHelper );
		
		ReleaseObject( mTextureShaderResourceView );
		DeleteObject( mMaterial );
		DeleteObject( mEffect );
		ReleaseObject( mVertexBuffer );
		ReleaseObject( mIndexBuffer );
	}

	void AcornModel::Initialize()
	{
		SetCurrentDirectory( Utility::ExecutableDirectory().c_str() );
		
		std::unique_ptr<Model> model( new Model( *mGame, "Content\\Models\\acorn.obj", true ) );

		// Initialize the material
		mEffect = new Effect( *mGame );
		mEffect->LoadCompiledEffect( L"Content\\Effects\\PhongReflectionModel.cso" );

		mMaterial = new PhongReflectionModelMaterial();
		mMaterial->Initialize( mEffect );
		
		techniqueIterator = mMaterial->GetEffect()->Techniques().begin();
		
		Mesh* mesh = model->Meshes().at( 0 );
		mMaterial->CreateVertexBuffer( mGame->Direct3DDevice(), *mesh, &mVertexBuffer );
		mesh->CreateIndexBuffer( &mIndexBuffer );
		mIndexCount = mesh->Indices().size();

		std::wstring textureName = L"Content\\Models\\acorn.png";
		HRESULT hr = DirectX::CreateWICTextureFromFile( mGame->Direct3DDevice(), mGame->Direct3DDeviceContext(), textureName.c_str(), nullptr, &mTextureShaderResourceView );
		if( FAILED( hr ) )
		{
			throw GameException( "CreateWICTextureFromFile() failed.", hr );
		}

		mKeyboard = (Keyboard*)mGame->Services().GetService( Keyboard::TypeIdClass() );
		assert( mKeyboard != nullptr );

		mRenderStateHelper = new RenderStateHelper( *mGame );

		mSpriteBatch = new SpriteBatch( mGame->Direct3DDeviceContext() );
		mSpriteFont = new SpriteFont( mGame->Direct3DDevice(), L"Content\\Fonts\\TimesNewRoman_12_bold.spritefont" );

		//place acorn
		XMVECTOR position = XMLoadFloat3( &mPosition );
		XMMATRIX worldMatrix = XMMatrixIdentity();
		MatrixHelper::SetTranslation( worldMatrix, mPosition );
		XMStoreFloat4x4( &mWorldMatrix, worldMatrix );
	}

	void AcornModel::Update( const GameTime& gameTime )
	{
		//UpdateAmbientLight( gameTime );
		//UpdateSpecularLight( gameTime );

		if( mKeyboard->WasKeyPressedThisFrame( DIK_SPACE ) )
		{
			Rendering::DrawMode drawMode;
			if( mDrawMode < AmbientMode )
			{
				drawMode = DrawMode( GetDrawMode() + 1 );
				SetDrawMode( drawMode );

				techniqueIterator++;
				mMaterial->SetCurrentTechnique( *techniqueIterator );
			}
			else
			{
				drawMode = (DrawMode)0;
				SetDrawMode( drawMode );

				techniqueIterator = mMaterial->GetEffect()->Techniques().begin();
				mMaterial->SetCurrentTechnique( *techniqueIterator );
			}
		}
	}

	void AcornModel::Draw( const GameTime& gameTime )
	{
		ID3D11DeviceContext* direct3DDeviceContext = mGame->Direct3DDeviceContext();
		direct3DDeviceContext->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST );

		Pass* pass = mMaterial->CurrentTechnique()->Passes().at( 0 );
		ID3D11InputLayout* inputLayout = mMaterial->InputLayouts().at( pass );
		direct3DDeviceContext->IASetInputLayout( inputLayout );

		UINT stride = mMaterial->VertexSize();
		UINT offset = 0;
		direct3DDeviceContext->IASetVertexBuffers( 0, 1, &mVertexBuffer, &stride, &offset );
		direct3DDeviceContext->IASetIndexBuffer( mIndexBuffer, DXGI_FORMAT_R32_UINT, 0 );

		XMMATRIX worldMatrix = XMLoadFloat4x4( &mWorldMatrix );
		XMMATRIX wvp = worldMatrix * mCamera->ViewMatrix() * mCamera->ProjectionMatrix();
		XMVECTOR ambientColor = XMLoadColor( &mAmbientColor );
		XMVECTOR specularColor = XMLoadColor( &mSpecularColor );

		//
		mMaterial->WorldViewProjection() << wvp;
		mMaterial->World() << worldMatrix;
		mMaterial->SpecularColor() << specularColor;
		mMaterial->SpecularPower() << mSpecularPower;
		mMaterial->AmbientColor() << ambientColor;
		mMaterial->LightColor() << mDirectionalLightSource.ColorVector();
		mMaterial->LightDirection() << mDirectionalLightSource.DirectionVector();
		mMaterial->ColorTexture() << mTextureShaderResourceView;
		mMaterial->CameraPosition() << mCamera->PositionVector();
		//

		pass->Apply( 0, direct3DDeviceContext );

		direct3DDeviceContext->DrawIndexed( mIndexCount, 0, 0 );

		//Rendering text bar
		mRenderStateHelper->SaveAll();
		mSpriteBatch->Begin();
		std::wostringstream helpLabel;
		helpLabel << std::setprecision( 2 ) << L"\n Draw Mode (SpaceBar): " << DrawModeString().c_str() << "\n";

		mSpriteFont->DrawString( mSpriteBatch, helpLabel.str().c_str(), XMFLOAT2( 0.0f, 50.0f ), DirectX::Colors::Gold );

		mSpriteBatch->End();
		mRenderStateHelper->RestoreAll();
		//End of text bar rendering

	}

	std::string AcornModel::DrawModeString() const
	{
		return DrawModeDisplayNames[(int)mDrawMode];
	}

	DrawMode AcornModel::GetDrawMode() const
	{
		return mDrawMode;
	}

	void AcornModel::SetDrawMode( Rendering::DrawMode drawMode )
	{
		mDrawMode = drawMode;
	}

	void AcornModel::UpdateAmbientLight( const GameTime& gameTime )
	{
		static float ambientIntensity = mAmbientColor.a;

		if( mKeyboard != nullptr )
		{
			if( mKeyboard->IsKeyDown( DIK_M ) && ambientIntensity < UCHAR_MAX )
			{
				ambientIntensity += LightModulationRate * (float)gameTime.ElapsedGameTime();
				mAmbientColor.a = (UCHAR)XMMin<float>( ambientIntensity, UCHAR_MAX );
			}

			if( mKeyboard->IsKeyDown( DIK_N ) && ambientIntensity > 0 )
			{
				ambientIntensity -= LightModulationRate * (float)gameTime.ElapsedGameTime();
				mAmbientColor.a = (UCHAR)XMMax<float>( ambientIntensity, 0 );
			}
		}
	}

	void AcornModel::UpdateSpecularLight( const GameTime & gameTime )
	{
		static float specularIntensity = mSpecularPower;

		if( mKeyboard != nullptr )
		{
			if( mKeyboard->IsKeyDown( DIK_INSERT ) && specularIntensity < UCHAR_MAX )
			{
				specularIntensity += LightModulationRate * (float)gameTime.ElapsedGameTime();
				specularIntensity = XMMin<float>( specularIntensity, UCHAR_MAX );

				mSpecularPower = specularIntensity;;
			}

			if( mKeyboard->IsKeyDown( DIK_DELETE ) && specularIntensity > 0 )
			{
				specularIntensity -= LightModulationRate * (float)gameTime.ElapsedGameTime();
				specularIntensity = XMMax<float>( specularIntensity, 0 );

				mSpecularPower = specularIntensity;
			}
		}
	}

}